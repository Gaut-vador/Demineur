package controller;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import game.Case;
import game.GameBoard;
import view.CaseView;

public class BoardController
{
   CaseView[][] _caseViewArray;
   int _size;
   GameBoard _gameBoard;
   private int _numberOfCaseDiscovered = 0;

   public BoardController(CaseView[][] caseViewArray, GameBoard pSquare)
   {
	  _caseViewArray = caseViewArray;
	  _size = _caseViewArray.length;
	  _gameBoard = pSquare;
   }

   public void buttonPressed(MouseEvent e, Case pCase)
   {
	  CaseView caseView = _caseViewArray[pCase.getXCoord()][pCase.getYCoord()];
	  if (pCase.isDiscovered())
	  {
		 return;
	  }
	  if (e.getButton() == MouseEvent.BUTTON1)
	  {
		 if (pCase.getCaseContent() == -1)
		 {
			revealAllBombes();
		 } else
		 {
			revealCase(pCase);
		 }
		 endGame(pCase);
	  } else if (e.getButton() == MouseEvent.BUTTON3)
	  {
		 // _case.setIcon(new ImageIcon("pictures/question.png"));
		 caseView.setText("?");
		 caseView.setForeground(Color.RED);
	  }
   }

   private void endGame(Case pCase)
   {
	  if (pCase.getCaseContent() == -1)
	  {
		 disableAllCase();
		 JOptionPane.showMessageDialog(null, "Sorry, but you lost", "Game Over", JOptionPane.PLAIN_MESSAGE);
	  } else if (isAllCaseDiscovered())
	  {
		 disableAllCase();
		 JOptionPane.showMessageDialog(null, "GG ! You won!", "Game Over", JOptionPane.PLAIN_MESSAGE);
	  }
   }

   private boolean isAllCaseDiscovered()
   {
	  System.out.println(_numberOfCaseDiscovered);
	  return _gameBoard.getNumberOfCaseToDiscover() == _numberOfCaseDiscovered;
   }

   private void disableAllCase()
   {
	  for (int i = 0; i < _size; i++)
	  {
		 for (int j = 0; j < _size; j++)
		 {
			_gameBoard.getCell(i, j).setDiscovered(true);
		 }
	  }
   }

   /**
    * Reveal all bombes on the field.
    */
   private void revealAllBombes()
   {
	  for (int i = 0; i < _size; i++)
	  {
		 for (int j = 0; j < _size; j++)
		 {
			if (_gameBoard.getCell(i, j).isBombe())
			{
			   ImageIcon icon = new ImageIcon("pictures/bomb.png");
			   Image img = icon.getImage();
			   Image newimg = img.getScaledInstance(_caseViewArray[i][j].getWidth(), _caseViewArray[i][j].getHeight(),
					 java.awt.Image.SCALE_SMOOTH);
			   icon = new ImageIcon(newimg);
			   _caseViewArray[i][j].setIcon(icon);
			   _caseViewArray[i][j].getLinkedCase().setDiscovered(true);
			}
		 }
	  }

   }

   /**
    * Reveal the given case. If the given case value is 0 then call itself with all
    * the adjacent cases.
    * 
    * @param pCase
    */
   private void revealCase(Case pCase)
   {
	  if (pCase.getCaseContent() == 0)
	  {
		 pCase.setDiscovered(true);
		 for (int x = -1; x <= 1; x++)
		 {
			for (int y = -1; y <= 1; y++)
			{
			   if ((x + pCase.getXCoord() >= 0 && x + pCase.getXCoord() < _size)
					 && (y + pCase.getYCoord() >= 0 && y + pCase.getYCoord() < _size) && !(x == 0 && y == 0))
			   {
				  Case currentCase = _caseViewArray[x + pCase.getXCoord()][y + pCase.getYCoord()].getLinkedCase();
				  if (!currentCase.isDiscovered())
				  {
					 currentCase.setDiscovered(true);
					 revealCase(currentCase);
				  }
			   }
			}
		 }
		 ImageIcon icon = new ImageIcon("pictures/sand.png");
		 Image img = icon.getImage();
		 Image newimg = img.getScaledInstance(_caseViewArray[pCase.getXCoord()][pCase.getYCoord()].getWidth(),
			   _caseViewArray[pCase.getXCoord()][pCase.getYCoord()].getHeight(), java.awt.Image.SCALE_SMOOTH);
		 icon = new ImageIcon(newimg);
		 _caseViewArray[pCase.getXCoord()][pCase.getYCoord()].setIcon(icon);
	  } else if (pCase.getCaseContent() > 0)
	  {
		 pCase.setDiscovered(true);
		 _caseViewArray[pCase.getXCoord()][pCase.getYCoord()].setText(String.valueOf(pCase.getCaseContent()));
	  }
	  _numberOfCaseDiscovered++;
   }
}
