package view;

import javax.swing.JButton;

import game.Case;

public class CaseView extends JButton
{
   private Case _linkedCase;

   public CaseView(Case pCase)
   {
	  setLinkedCase(pCase);
   }

   public Case getLinkedCase()
   {
	  return _linkedCase;
   }

   public void setLinkedCase(Case _linkedCase)
   {
	  this._linkedCase = _linkedCase;
   }

   /**
    * 
    */
   private static final long serialVersionUID = 42;

}
